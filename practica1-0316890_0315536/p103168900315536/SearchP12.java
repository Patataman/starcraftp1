package org.iaie.practica1.p103168900315536;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.iaie.search.Successor;
import org.iaie.search.algorithm.Astar;

import jnibwapi.JNIBWAPI;
import jnibwapi.Map;
import jnibwapi.Position;
import jnibwapi.Position.PosType;

public class SearchP12 extends Astar {

	public SearchP12(JNIBWAPI bwapi) {
		super(bwapi.getMap());
	}

	/**
	 * Genera los sucesores válidos adyacentes en vertical, horizontal y diagonal dada una posición.
	 * Se comprueba si las casillas en cada una de los direcciones son válidas para caminar
	 * se añaden a la lista que se devuelve.
	 * 
	 * @return List<Successor>: Lista de casillas adyacentes válidas
	 */
	public List<Successor> generateSuccessors(Point actualState) {
		//Lista de nodos sucesores
		List<Successor> retu = new ArrayList<Successor>();
		
		//Casilla a la derecha
		if (((Map)this.map).isWalkable(new Position(actualState.x+1, actualState.y, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x+1, actualState.y)));
		}
		//Casilla a la izquierda
		if (((Map)this.map).isWalkable(new Position(actualState.x-1, actualState.y, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x-1, actualState.y)));		
		}
		//Casilla arriba
		if (((Map)this.map).isWalkable(new Position(actualState.x, actualState.y-1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x, actualState.y-1)));
		}
		//Casilla hacia abajo
		if (((Map)this.map).isWalkable(new Position(actualState.x, actualState.y+1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x, actualState.y+1)));
		}
		//Diagonal arriba drch
		if (((Map)this.map).isWalkable(new Position(actualState.x-1, actualState.y-1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x-1, actualState.y-1)));
		}
		//Diagonal abajo drch
		if (((Map)this.map).isWalkable(new Position(actualState.x-1, actualState.y+1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x-1, actualState.y+1)));
		}
		//Diagonal abajo izquierda
		if (((Map)this.map).isWalkable(new Position(actualState.x+1, actualState.y+1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x+1, actualState.y+1)));
		}
		//Diagonal arriba izquierda
		if (((Map)this.map).isWalkable(new Position(actualState.x+1, actualState.y-1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x+1, actualState.y-1)));
		}
		
		return retu;
	}

	/**
	 * Calcula la heurística de camino marcado entre el punto inicial y el punto final
	 * Los costes serán 1 para movimientos horizontales y verticales y 0.5 para movimientos diagonales
	 * @param state: Punto inicial
	 * @param goalState: Casilla a la que se quiere mover
	 * @return double: Heuristica calculada.
	 */
	public double calculateheuristic(Point state, Point goalState) {
		//En este caso como se contemplan todos los tipos de movimientos
		//la medida más adecuada en este caso es una Distancia Euclídea.
		
		//Sin embargo los costes serán distintos para movimientos horizontales/verticales
		//y movimientos diagonales, por lo que hay que comprobar el tipo de movimiento
		//que se quiere realizar.
		
		double coste;
		
		//Distancia euclídea: Raiz de la suma de los cuadrados de las diferencias horizontales y verticales
		double dist = Math.sqrt(Math.pow(state.getX() - goalState.getX(),2)+(Math.pow(state.getY() - goalState.getY(),2)));
		
		//Si cualquiera de los dos ejes están a la misma altura, quiere decir que el
		//movimiento es horizontal o vertical.
		if ((state.getX() - goalState.getX()) == 0 || (state.getY() - goalState.getY()) == 0){
			coste = 1;
		} else {
			//Movimientos diagonales, coste 0.5
			coste = 0.5;
		}
		
		
		return dist + coste;
	}

}
