package org.iaie.practica1.p103168900315536;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.iaie.search.Result;
import org.iaie.search.Successor;
import org.iaie.search.algorithm.HierarchicalSearch;

import jnibwapi.ChokePoint;
import jnibwapi.JNIBWAPI;
import jnibwapi.Map;
import jnibwapi.Position;
import jnibwapi.Region;
import jnibwapi.Position.PosType;

public class HSearch extends HierarchicalSearch {

	private JNIBWAPI bwapi;

	public HSearch(JNIBWAPI bwapi){
		super();
		this.bwapi = bwapi;
	}
	
	//No hay que implementarlo
	public List<Successor> generateSuccessor(Point actualState) {
		// TODO Auto-generated method stub
		return null;
	}

	//No hay que implementarlo
	public int calculateheuristic(Point state, Point goalState) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Dados los puntos de origen y destino, se calcula el camino a seguir
	 * desplazandose en cada region hasta el chokepoint de la seguiente.
	 * @param start: Origen del proceso de búsqueda
	 * @param end: Destino/Meta.
	 */
	@SuppressWarnings("null")
	public Result search(Point start, Point end) {
		// Creamos los objetos necesarios para la búsqueda
		SearchP13 p13 = new SearchP13(this.bwapi);
		SearchP12 p12 = new SearchP12(this.bwapi);
		// Obtenemos el centro de la última región para que funcione correctamente
		Region initRegion = (p13.getM()).getRegion(new Position(end.x, end.y, PosType.WALK));
		// Utilizamos la búsqueda entre regiones para obtener el camino y las variables necesarias
		Result resultado = p13.search(start, new Point(initRegion.getCenter().getWX(), initRegion.getCenter().getWY()));
		List<Point> puntosRegiones = resultado.getPath(); //Lista de regiones a recorrer
		List<Integer> idRegiones = new ArrayList<Integer>();
		List<Point> myPath = new ArrayList<Point>();
		for (Point pr : puntosRegiones){ // Obtenemos las regiones por id
			idRegiones.add((p13.getM()).getRegion(new Position((int)pr.getX(), (int)pr.getY(), PosType.WALK)).getID()-1);
		}
		
		// Añadimos a myPath todos los puntos que forman el camino
		Position destinoPosition;
		myPath.add(start);
		for(int i = 1; i<idRegiones.size()-1; i++){
			destinoPosition = p13.getAux().getChokeCenter(idRegiones.get(i), idRegiones.get(i+1));
			myPath.add(new Point(destinoPosition.getWX(), destinoPosition.getWY()));
		}
		myPath.add(end);
		// Creamos las variables que vamos a necesitar para crear el objeto result		
		List<Point> resultPath = new ArrayList<Point>();
		resultPath.add(start);
		resultPath.add(end);
		int generated = 0;
		int expanded = 0;
		int cost = 0;
		long time = 0;
		// imprimimos los resultados de la búsqueda de regiones
		System.out.println("Búsqueda de regiones:");
		resultado.print();
		int expandedRegion = resultado.getExpandedNodes(); // Guardamos este valor para la memoria
		
		// Ahora se llama al método search de Searchp12 por cada par de puntos del camino y conteamos sus resultados
		for (int i = 0; i<myPath.size()-1; i++){
			resultado = p12.search(myPath.get(i), myPath.get(i+1));
			generated += resultado.getGeneratedNodes();
			expanded += resultado.getExpandedNodes();
			cost += resultado.getCost();
			time += resultado.getTime();
			resultado.print();
		}
		// Añadimos la impresión de la traza que meteremos en la memoria y contruimos el objeto a devolver por este método. 
		System.out.println("("+ start.x + ", " + start.y + ") & (" + end.x + ", " + end.y + ") & " + expandedRegion + " & " + expanded + " & " + cost + " & " + time +  " \\\\");
		return new Result(resultPath, generated, expanded, cost, time);
	}

}
