package org.iaie.practica1.p103168900315536;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.iaie.search.Successor;
import org.iaie.search.algorithm.Astar;

import jnibwapi.JNIBWAPI;
import jnibwapi.Map;
import jnibwapi.Position;
import jnibwapi.Position.PosType;

public class SearchP11 extends Astar {
	
	public SearchP11(JNIBWAPI bwapi) {
		super(bwapi.getMap());
	}

	/**
	 * Genera los sucesores válidos adyacentes en vertical y horizontal dada una posición.
	 * Se comprueba arriba, abajo, derecha e izquierda las casillas, si son válidas para caminar
	 * se añaden a la lista que se devuelve.
	 * 
	 * @return List<Successor>: Lista de casillas adyacentes válidas
	 */
	public List<Successor> generateSuccessors(Point actualState) {
		//Lista de nodos sucesores
		List<Successor> retu = new ArrayList<Successor>();
			
		//Casilla a la derecha
		if (((Map)this.map).isWalkable(new Position(actualState.x+1, actualState.y, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x+1, actualState.y)));
		}
		//Casilla a la izquierda
		if (((Map)this.map).isWalkable(new Position(actualState.x-1, actualState.y, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x-1, actualState.y)));		
		}
		//Casilla arriba
		if (((Map)this.map).isWalkable(new Position(actualState.x, actualState.y-1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x, actualState.y-1)));
		}
		//Casilla hacia abajo
		if (((Map)this.map).isWalkable(new Position(actualState.x, actualState.y+1, PosType.WALK))) {
			retu.add(new Successor(new Point(actualState.x, actualState.y+1)));
		}
		
		return retu;
	}

	/**
	 * Calcula la heurística de camino marcado entre el punto inicial y el punto final
	 * @param state: Punto inicial
	 * @param goalState: Casilla a la que se quiere mover
	 * @return double: Heuristica calculada.
	 */
	public double calculateheuristic(Point state, Point goalState) {
		//Dado que sólo se consideran casillas verticales y horizontales, lo más
		//adecuado para este cálculo es una Distancia Manhattan ya se calcula 
		//en función de casillas horizontales y verticales.
		
		//COSTE = 1
		double distX = Math.abs(state.getX() - goalState.getX());
		double distY = Math.abs(state.getY() - goalState.getY());
		
		//f(n) = g(n)+h(n), en este caso g(n) es 1 y h(n) es distX+distY 
		return distX + distY + 1;
	}

}
