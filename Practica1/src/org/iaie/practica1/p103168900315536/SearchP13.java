package org.iaie.practica1.p103168900315536;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.iaie.search.Successor;
import org.iaie.search.algorithm.Astar;

import jnibwapi.JNIBWAPI;
import jnibwapi.Map;
import jnibwapi.Position;
import jnibwapi.Position.PosType;
import jnibwapi.Region;

import org.iaie.practica1.p103168900315536.HierarchicalMap;

public class SearchP13 extends Astar {
	
	private HierarchicalMap aux;
	private Map m;

	public SearchP13(Object bwapi) {
		super(bwapi);
		aux = new HierarchicalMap(((JNIBWAPI)bwapi).getMap());
		m = ((JNIBWAPI)bwapi).getMap();
	}

	/**
	 * Genera las regiones sucesoras dada una región.
	 * @param actualRegion: Indica la región actual
	 */
	public List<Successor> generateSuccessors(Point actualRegion) {
		Region myRegion = (getM()).getRegion(new Position(actualRegion.x, actualRegion.y, PosType.WALK));
		return getAux().getConexiones().get(myRegion.getID()-1); //Para que el id coincida con el indice tiene que restarsele 1
	}

	/**
	 * Genera un valor estimado del coste de desplazarse desde la
	 * región actual hasta la región destino
	 * @param region: Región origen
	 * @param goalRegion: Región destino
	 */
	public double calculateheuristic(Point region, Point goalRegion) {
		Position origen = new Position(region.x, region.y, PosType.WALK);
		Position destino= new Position(region.x, region.y, PosType.WALK);
		if (m.getRegion(origen).getID() == (m.getRegion(destino).getID())) {
			return 0;
		} else {
			double dist = Math.sqrt(Math.pow(region.getX() - goalRegion.getX(),2)+(Math.pow(region.getY() - goalRegion.getY(),2)));
			return dist + 1;
			
		}
	}

	public HierarchicalMap getAux() {
		return aux;
	}

	public void setAux(HierarchicalMap aux) {
		this.aux = aux;
	}

	public Map getM() {
		return m;
	}

	public void setM(Map m) {
		this.m = m;
	}
}
