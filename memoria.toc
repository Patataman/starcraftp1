\select@language {spanish}
\contentsline {section}{Introducci\IeC {\'o}n}{3}{section*.2}
\contentsline {section}{\numberline {1}B\IeC {\'u}squeda en cuadriculas}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}generateSuccessors}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}calculateHeuristic}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Resultados}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}B\IeC {\'u}squeda en cuadriculas en diagonales}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}generateSuccessors}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}calculateHeuristic}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Resultados}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}B\IeC {\'u}squeda jer\IeC {\'a}rquica}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}HierarchicalMap}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}generateSuccessors}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}calculateHeuristic}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}search}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Resultados}{6}{subsection.3.5}
\contentsline {section}{\numberline {4}Conclusiones}{6}{section.4}
